import React from 'react'
import { hot } from 'react-hot-loader'
import logo from '../assets/images/logo.png'
import './App.css'
import { BrowserRouter as Router, Route, Link, Redirect } from "react-router-dom";
import { Navbar, Nav, NavItem, NavDropdown, MenuItem, FormGroup, ControlLabel, FormControl, HelpBlock, Button, Switch } from 'react-bootstrap'
import axios from 'axios';
import { Form, Text, Radio, RadioGroup, TextArea, Checkbox } from 'react-form';
import { observable } from 'mobx'
import { observer } from 'mobx-react'

const FieldGroup = ({id, label, help, placeholder, cyData , inputData}) => (
  <FormGroup controlId={id}>
    <ControlLabel>{label}</ControlLabel>
    <FormControl placeholder={placeholder} data-cy={cyData} inputRef={input => inputData = input} />
    {help && <HelpBlock>{help}</HelpBlock>}
  </FormGroup>
)
let formInputData = observable({
  username: '',
  password: ''
})

let loginState = observable({
  isLogin: false,
  accessToken: '',
  refreshToken: ''
})

@observer
class SignIn extends React.Component {
  constructor(props) {
    super(props);
    this.state = {value: '', isLogin: false};

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    this.setState({value: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    axios.post("http://192.168.0.24:3000/oauth/token", {
        'grant_type': 'password',
        'username': formInputData.username.value,
        'password': formInputData.password.value
      }, {
      headers: {
        'Accept': "application/json; version=1"
      }
    })
    .then(function (response) {
      loginState.isLogin = true
      loginState.loginText = 'Sign Out'
      loginState.url = '/logout'
      loginState.accessToken = response.data.access_token
      loginState.refreshToken = response.data.refresh_token
    })
    .catch(function (error) {
      console.log(error);
    });
  }

  render () {
    if (loginState.isLogin) {
      return <Redirect to="/blog" />
    }
    return (
      <div>
        <form onSubmit={this.handleSubmit}>
          <FormGroup controlId="username">
            <ControlLabel>Username</ControlLabel>
            <FormControl placeholder="Username" data-cy="username" inputRef={input => formInputData.username = input} />
          </FormGroup>
          <FormGroup controlId="password">
            <ControlLabel>Password</ControlLabel>
            <FormControl type="password" placeholder="Password" data-cy="password" inputRef={input => formInputData.password = input} />
          </FormGroup>
          <input type="submit" data-cy="login_btn" className="btn btn-primary" value="Submit" />
        </form>
      </div>
    );
  }
}

const Blog = () => (
  <div>
    <h2>About</h2>
  </div>
);

class LogOut extends React.Component{
  constructor(props) {
    super(props);
    loginState.isLogin = false
    loginState.accessToken = ''
    loginState.refreshToken = ''
  }

  render() {
    return <Redirect to="/signin" />
  }
}

@observer
class TopNav extends React.Component{
  signInBtn() {
    if (!loginState.isLogin) {
      return (
        <Link data-cy="link_to_signin" to="/signin">
          Sign In
        </Link>);
    }else{
      return (
        <Link data-cy="link_to_signout" to="/logout">
          Sign Out
        </Link>)
    }
  }

  render() {
    return (
      <Router>
        <div>
          <Navbar>
            <Navbar.Header>
              <Navbar.Brand>
                <Link to="/blog">Blog</Link>
              </Navbar.Brand>
            </Navbar.Header>
            <Nav>
              <NavItem>
                {this.signInBtn()}
              </NavItem>
            </Nav>
          </Navbar>

          <Route path="/blog" component={Blog} />
          <Route path="/signin" component={SignIn} />
          <Route path="/logout" component={LogOut} />
        </div>
      </Router>
    )
  }
}

const Home = () => (
  <Router>
    <h1>Home</h1>
  </Router>
)

const App = () => (
  <div>
    <TopNav/>
  </div>
)
export default hot(module)(App)
